<?php

require 'vendor/autoload.php';

use Dzava\Lighthouse\Lighthouse;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

try{
    (new Lighthouse())
        ->setLighthousePath('./node_modules/lighthouse/lighthouse-cli/index.js')
        ->setOutput('report.json')
        ->bestPractices()
        ->performance()
        ->seo()
        ->audit('https://etus.com.br/');
}
catch(Exception $e){

    echo $e->getMessage();

}